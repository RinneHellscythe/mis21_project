class CashiersController < ApplicationController
  before_action :authenticate_admin!, only: [:new, :edit, :update, :create, :destroy, :index, :show]
  def new
  end
  def create
    @cashier = Cashier.new(params[:cashier].permit(:email, :name, :password, :status))
    @cashier.password = :password
    @cashier.status = true
    @cashier.save
    redirect_to @cashier
  end
  def edit
    @cashier = Cashier.find(params[:id])
  end
  def update
    # go to edit.html.erb, do some stuff with instance @product, bring them here then do "@product.update"
    # ".update" is a built-in Rails function
    @cashier = Cashier.find(params[:id])
    if @cashier.update(cashier_params)
      redirect_to @cashier
    else
      render 'edit'
    end
  end
  def destroy
    @cashier = Cashier.find(params[:id])
    @cashier.destroy
    redirect_to cashiers_path
  end
  def show
    @cashier = Cashier.find(params[:id])
  end
  def index
    @cashiers = Cashier.all
  end
  private
    def cashier_params
        params.require(:cashier).permit(:name, :email, :encrypted_password, :status)
    end
end
