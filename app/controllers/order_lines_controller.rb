class OrderLinesController < ApplicationController
    def new
    end
    def create
        
    end
    def show
        @order_lines = OrderLine.all
    end
    def index
        @order_lines = OrderLine.all
    end
    def destroy
        @order_lines = OrderLine.find(params[:id])
        @order_lines.destroy
        redirect_to order_line_index_path
    end
    private
    def order_line_params
        params.require(:order_line).permit(:product_id_id, :quantity, :price)
    end
    #def order_params
    #    params
    #        .require(:order)
    #        
    #end
end
