class ProductsController < ApplicationController
    before_action :authenticate_admin!, only: [:new, :edit, :update, :create, :destroy, :index, :show]
    # ^^^ means that only an authenticated admin can do the following command
    layout "application"
    def new
    end
    def edit
        @product = Product.find(params[:id])
    end
    def update
        # go to edit.html.erb, do some stuff with instance @product, bring them here then do "@product.update"
        # ".update" is a built-in Rails function
        @product = Product.find(params[:id])
        if @product.update(product_params)
            redirect_to @product
        else
            render 'edit'
        end
    end
    def create
        #render plain: params[:product].inspect
        #^^^ this just prints the stuff from the "new" form
        @product = Product.new(params[:product].permit(:name, :price, :status))
        #^^^ ".permit" is related to Rails' security stuff, this allows the app to permit its own attributes
        @product.status = true
        #^^^ status is a paremeter made with the Product model, it's set to true because we want the product to be available immediately
        @product.save
        #^^^ ".save" saves the model to the database
        redirect_to @product
    end
    def destroy
        @product = Product.find(params[:id])
        @product.destroy
        redirect_to products_path
    end
    def show
        @product = Product.find(params[:id])
        #^^^ "params[:id] is there to find the Product with an id that the app is interested in"
    end
    def index
        @products = Product.all
        #^^^ the code to list all product, it is very important that the "@" variable is PLURAL
    end
    private
        def product_params
            params.require(:product).permit(:name, :price, :status)
        end
end

#new and create are built-in on Rails, they exist automatically via "rails generate"
#the built-in commands always refer to their respective html files: "<controller name>/<new/ create/ etc,>.html.erb"
# ex: "new" on a Products Controller refers to the new.html.erb on views/products

#Rails models are automatically initialized with its attributes
