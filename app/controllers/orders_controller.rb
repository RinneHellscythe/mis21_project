class OrdersController < ApplicationController
    before_action :authenticate_admin!, only: [:new, :edit, :update, :create, :destroy, :index, :show]
    def new
        @order = Order.new
    end
    def edit
    end
    def update
    end
    def create
        @order = Order.new(params[:order].permit(:cashier_id, :total_price, :cash))
        @order.save
        redirect_to @order
    end
    def index
        @orders = Order.all
    end
    def destroy
        @order = Order.find(params[:id])
        @order.destroy
        redirect_to controller: 'orders'
    end
    def show
        @order = Order.find(params[:id])
    end
    private
    def order_params
        params.require(:order).permit(:cashier_id, :total_price, :cash)
    end
end
