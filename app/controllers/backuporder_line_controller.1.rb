class OrderLineController < ApplicationController
    def new
        @order_line = OrderLine.new
        @products = Product.all
    end
    def create
        if params[:finish]
            @order_line = OrderLine.new(params[:order_line].permit(:product_id_id, :quantity, :price))
            @order_line.price = Product.find(@order_line.product_id_id).price
            @order_line.save
            redirect_to new_order_path
        else
            @order_line = OrderLine.new(params[:order_line].permit(:product_id_id, :quantity, :price))
            @order_line.price = Product.find(@order_line.product_id_id).price
            @order_line.save
            redirect_to new_order_line_path
        end
    end
    def show
        @order_lines = OrderLine.all
    end
    def index
        @order_lines = OrderLine.all
    end
    def destroy
        @order_lines = OrderLine.find(params[:id])
        @order_lines.destroy
        redirect_to order_line_index_path
    end
    private
    def order_line_params
        params.require(:order_line).permit(:product_id_id, :quantity, :price)
    end
    #def order_params
    #    params
    #        .require(:order)
    #        
    #end
end
