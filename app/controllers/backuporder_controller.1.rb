class OrderController < ApplicationController
    before_action :authenticate_admin!, only: [:new, :edit, :update, :create, :destroy, :index, :show]
    def new
        @order_line = OrderLine.new
        @products = Product.all
        @order = Order.new
    end
    def edit
    end
    def update
    end
    def create
        @order = Order.new(params[:order].permit(:cashier_id_id, :total_price, :cash))
        @order.save
        OrderLine.delete_all
        redirect_to '/'
    end
    def index
        @orders = Order.all
    end
    def destroy
        @order = Order.find(params[:id])
        @order.destroy
        redirect_to order_index_path
    end
    def show
    end
    private
    def order_params
        params.require(:order).permit(:cashier_id_id, :total_price, :cash)
    end
end
