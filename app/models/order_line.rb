class OrderLine < ActiveRecord::Base
  belongs_to :order
  #has_many :product_id
  has_many :products
  attr_accessor :product, :quantity, :price
  #accepts_nested_attributes_for :products, 
  #                              allow_destroy: true,
  #                              :reject_if => :all_blank
  #                              #reject_if: proc { |att| att['description'].blank? }
end