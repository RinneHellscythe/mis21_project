class Order < ActiveRecord::Base
  belongs_to :cashier
  has_many :order_lines
  attr_accessor :total_price, :cash, :cashier, :order_line_attributes
  accepts_nested_attributes_for :order_lines, 
                                allow_destroy: true,
                                :reject_if => :all_blank
end
