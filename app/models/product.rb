class Product < ActiveRecord::Base
    belongs_to :admin
    belongs_to :order_line
    accepts_nested_attributes_for :order_line
end
