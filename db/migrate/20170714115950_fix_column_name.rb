class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :orders, :cashier_id_id, :cashier
    rename_column :order_lines, :product_id_id, :product
    rename_column :order_lines, :order_id_id, :order
  end
end
