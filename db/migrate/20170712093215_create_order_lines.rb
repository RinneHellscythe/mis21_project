class CreateOrderLines < ActiveRecord::Migration
  def change
    create_table :order_lines do |t|
      t.references :product_id, index: true, foreign_key: true
      t.references :order_id, index: true, foreign_key: true
      t.integer :quantity
      t.decimal :price

      t.timestamps null: false
    end
  end
end
