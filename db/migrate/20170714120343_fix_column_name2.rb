class FixColumnName2 < ActiveRecord::Migration
  def change
    rename_column :orders, :cashier, :cashier_id
    rename_column :order_lines, :product, :product_id
    rename_column :order_lines, :order, :order_id
    end
end
