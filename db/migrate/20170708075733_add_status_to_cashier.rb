class AddStatusToCashier < ActiveRecord::Migration
  def change
    add_column :cashiers, :status, :boolean
  end
end
