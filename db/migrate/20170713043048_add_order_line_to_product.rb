class AddOrderLineToProduct < ActiveRecord::Migration
  def change
    add_reference :products, :order_line, index: true, foreign_key: true
  end
end
