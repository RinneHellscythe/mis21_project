class AddStatusToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :status, :string
    add_index :admins, :status, unique: true
  end
end
