class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :cashier_id, index: true, foreign_key: true
      t.decimal :total_price
      t.decimal :cash

      t.timestamps null: false
    end
  end
end
